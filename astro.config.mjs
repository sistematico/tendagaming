import { defineConfig } from 'astro/config';

export default defineConfig({
  site: 'https://tendagaming.com.br',
  outDir: 'public',
  publicDir: 'static',
});
